import sqlite3
from sqlite3 import Error

try:
    con = sqlite3.connect('panamenismosbd.db')
    print("Conexion Exitosa")
except Error:
    print ("Conexion Fallida")

try:
    cur = con.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS Panamenismos(id integer PRIMARY KEY, palabra text, significado text)")
    print("Creacion exitosa")
except Error:
    print("Error en creacion")

try:
   cur = con.cursor()
   cur.execute("INSERT INTO Panamenismos VALUES(1, 'Que sopa', 'Como estas?')")
   cur.execute("INSERT INTO Panamenismos VALUES(2, 'Meto', 'expresión chiricana que se expone a diferentes situaciones')")
   cur.execute("INSERT INTO Panamenismos VALUES(3, 'Pollo(a)', 'Novio/Novia')")
   cur.execute("INSERT INTO Panamenismos VALUES(4, 'Pelo el bollo', 'Se murio')")
   con.commit()
   print("Insercion exitosa")
except Error:
   print("Error en insercion")



def palabranueva (id, palabra, significado):
    try:
        cur = con.cursor()
        cur.execute('''INSERT INTO Panamenismos VALUES(?, ?, ?)''', (id, palabra, significado))
        con.commit()
        print("Insercion exitosa")
    except Error:
        print("Error en insercion. Id existente")
        #con.close()

def modificar(palabra, id):
    try:
       cur = con.cursor()
       cur.execute('UPDATE Panamenismos SET palabra = ? WHERE id = ?',(palabra,id))
       con.commit()
       print("Actualizacion exitosa")
    except Error:
       print("Error al actualizar")
       #con.close()
       
def borrar(id):
    try:
       cur = con.cursor()
       cur.execute('DELETE FROM Panamenismos WHERE id = %s' %id)
       con.commit()
       print("Registro Eliminado")
    except Error:
       print("Error al eliminar")
       #con.close()

def mostrar():
    try:
       cur = con.cursor()
       cur.execute('SELECT * FROM Panamenismos')
       palabraspan = cur.fetchall()
       for Panamenismos in palabraspan:
            print(Panamenismos)
    except Error:
       print("Error al traer datos")
       #con.close

def buscar(palabra, id):
     try:
       cur = con.cursor()
       cur.execute('SELECT significado FROM Panamenismos WHERE palabra=? and id=?',(palabra,id))
       palabraspan = cur.fetchall()
       for Panamenismos in palabraspan:
            print("El signifaco es: ", Panamenismos)
     except Error:
       print("Error al traer datos")
       #con.close()

import os
def menu():
    #os.system("cls")
    print ()
    print ()
    print ("Jerga Panamenas")
    print ("palabras que se usan en Panamá mas que en otros países en la vida cotidiana.")
    print ("Selecciona una opcion del siguiente menu:")
    print ("1) AGREGA UNA NUEVA PALABRA")
    print ("2) EDITA UNA PALABRA EXISTENTE")
    print ("3) ELIMINA UNA PALABRA EXISTENTE")
    print ("4) VER LISTADO DE PALABRAS")
    print ("5) BUSCAR SIGNIFICADO DE PALABRA")
    print ("6) SALIR")
    print ()
    

while True:
    menu()

# Leemos lo que ingresa el usuario
    opcion = input("Selecciona :")
    print ()
    if opcion == "1":
        print ("Seleccionaste la opcion que agrega una palabra nueva")
        id = int(input("ID: "))
        palabra = input("Palabra: ")
        significado = input("Signidicado: ")
        palabranueva(id, palabra, significado)
        
    elif opcion == "2":
        print ("Seleccionaste la opcion que modifica una Palabra")
        id = int (input ("Ingresa un ID: "))
        palabra = input ("Ingresa la palabra actualizada: ")
        modificar(palabra, id)
    elif opcion == "3":
        print ("Seleccionaste la opcion que borra una palabra")
        id = int (input ("Ingresa el ID de la palabra que deseas eliminar: ")) 
        borrar(id)
                   
    elif opcion == "4":
        print ("Seleccionaste la opcion que nos muestra todos las palabras en la tabla")
        mostrar()
                
    elif opcion == "5":
        print ("Seleccionaste la opcion que nos busca el significado de la palabra")
        id = int (input ("Ingresa un ID de la palabra que quieras saber su significado: "))
        palabra = input ("Ingresa la palabra que quieres saber su significado: ")
        buscar(palabra,id)
        
    elif opcion == "6":
        print ("Si quieres aprender mas Panamanismos vuelve pronto")
        exit()
    else:
        print("opcion invalida")

   
